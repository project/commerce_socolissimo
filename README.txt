Commerce Colissimo
---------------------------------

This module extend the Drupal Commerce Shipping methods, and implement
Colissimo methods from La Poste.

Various services provides by this method are only available in France and 
Belgium.

Implemented services:
* Home delivery (FR only)
* Appointment (FR only)
* Cityssimo (FR only)
* Local store
* Post office

Permissions:
Services rates management and module settings require the `administer shipping`
permission.

Configuration:
 * admin/commerce/config/socolissimo-flexibility
 * Make sure that the default home delivery shipping code is set to the correct
   value (defaults to DOM). (Only useful when exporting data to INET Expeditor.)

Learn more about Colissimo:
 * http://www.colissimo.fr/so/So_Colissimo.jsp
